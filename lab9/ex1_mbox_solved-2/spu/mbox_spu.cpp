#include <stdio.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>

#define SIZE 80
 
int main(unsigned long long speid, unsigned long long argp, unsigned long long envp){
	uint32_t index = 0, i, res ;
	unsigned int data[SIZE] __attribute__((aligned(16)));

	printf("[SPU 0x%llx] started\n", speid);

	argp = argp; //silence warnings
	envp = envp; //silence warnings

	for (i=0; i<SIZE; i++) {
		data[i] = (i + 1) * (i + 1);
	};

	//TODO: Task3 
	while(1) {
		//TODO: Task1 - trimite un index de la PPU la SPU
		index = spu_read_in_mbox();
		printf("[SPU 0x%llx] Received index %d\n", speid, (int)index);
		//TODO: Task2 - raspunde cu elementul din vector de la SPU la PPU
		res = ((index <= SIZE) ? data[index] : 0);
		spu_write_out_intr_mbox(res);
		printf("[SPU 0x%llx] Sent value %d\n", speid, res);
		if (res == 0)
			break;
	};
	
	printf("[SPU 0x%llx] finished\n", speid);

	return 0;
}



