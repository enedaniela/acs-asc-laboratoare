"""
Objectives:
- Lists
- Dict
- constructs: loops, conditionals
- more work with files
- functions
"""

#TODO implement the following functions

# function #1
"""
    Returns the number of lines in a file given as parameter.
    @param filename: the file's name
"""
def f1(filename):
    count = 0
    with open(filename, 'r') as f:
        for line in f:
            count = count + 1
    f.close()
    return count

# function #2
"""
    Reads the content of a file and fills the given list with the sentences
    found in the file
    @param filename: the file's name
    @param sentences: the list that will be contain the sentences
"""
def f2(filename, list):
    with file(filename) as f:
        s = f.read()
    sentences = s.split('.')
    for i in range(len(sentences)):
        list.append(sentences[i])
    f.close()
# function #3
"""
    Return a list of the top N most used words in a given file
    @param filename: the file's name
    @param n: the number of words in the top, default is 5
"""
def f3(filename,ni):
    with file(filename) as f:
        s = f.read()
    words = s.split()
    dict = {}
    for w in words:
        dict[w] = 0
    for w in words:
        dict[w] = dict[w] + 1
    sorted_dict = sorted(dict, key = dict.get, reverse = True)[:ni]
    f.close()
    return sorted_dict
#bonus 1p: implement your own sort method instead of using an existing one



if __name__ == "__main__":

    filename = "fisier_input"

    # TODO test the functions
    nr_lines = f1(filename)
    print nr_lines
    # TODO print all the sentences with less than 15 words
    sentences = []
    f2(filename, sentences)
    for i in sentences:
        nr_words = len(i.split())
        if nr_words < 15:
            print i
    # TODO write the most used words in a file, one per line
    most_used = f3(filename, 1)
    print most_used