#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <libspe2.h>
#include <pthread.h>
#include <libmisc.h> // malloc_align & free_align
#include "../lab8_common.h"

extern spe_program_handle_t lab8_spu;

void *ppu_pthread_function(void *thread_arg) {

	spe_context_ptr_t ctx;
	pointers_t *arg = (pointers_t *) thread_arg;

	/* Create SPE context */
	if ((ctx = spe_context_create (0, NULL)) == NULL) {
		perror ("Failed creating context");
		exit (1);
	}

	/* Load SPE program into context */
	if (spe_program_load (ctx, &lab8_spu)) {
		perror ("Failed loading program");
		exit (1);
	}

	/* Run SPE context */
	unsigned int entry = SPE_DEFAULT_ENTRY;
	/* TODO: transferati prin argument adresa pentru transferul DMA initial (vezi cerinta 2) */
	if (spe_context_run(ctx, &entry, 0, (void*)arg, NULL, NULL) < 0) {  
		perror ("Failed running context");
		exit (1);
	}

	/* Destroy context */
	if (spe_context_destroy (ctx) != 0) {
		perror("Failed destroying context");
		exit (1);
	}

	pthread_exit(NULL);
}

int main()
{
	float *A, *B, *C, *D;
	unsigned int i, spu_threads = NUM_SPU_THREADS;
	pthread_t threads[NUM_SPU_THREADS];
	pointers_t thread_arg[NUM_SPU_THREADS] __attribute__ ((aligned(16)));

	A = (float*) malloc_align(PPU_ARR_SIZE * sizeof(float), 4);
	B = (float*) malloc_align(PPU_ARR_SIZE * sizeof(float), 4);
	C = (float*) malloc_align(PPU_ARR_SIZE * sizeof(float), 4);
	D = (float*) malloc_align(PPU_ARR_SIZE * sizeof(float), 4);

	if (!A || !B || !C || !D) {
		printf("Failed alloc: %p %p %p %p\n", A, B, C, D);
		exit(0);	
	}

	/*
	 * Initialization
	 */
	for (i=0;i<PPU_ARR_SIZE;i++) {
		A[i] = (float)(i % 4);
		B[i] = (float)(3 - i % 4);
		C[i] = 0;
		D[i] = 0;
	}

	/* 
	 * Create several SPE-threads to execute 'lab8_spu'.
	 */

	for(i = 0; i < spu_threads; i++) {

		/* TODO: populati cate o structura de tip pointers_t pentru fiecare thread in parte (vezi cerinta 2) */
		thread_arg[i].A = A + i * PPU_ARR_SIZE / NUM_SPU_THREADS;
		thread_arg[i].B = B + i * PPU_ARR_SIZE / NUM_SPU_THREADS;
		thread_arg[i].C = C + i * PPU_ARR_SIZE / NUM_SPU_THREADS;
		thread_arg[i].D = D + i * PPU_ARR_SIZE / NUM_SPU_THREADS;

		/* Create thread for each SPE context */
		if (pthread_create (&threads[i], NULL, &ppu_pthread_function, &thread_arg[i]))  {
			perror ("Failed creating thread");
			exit (1);
		}
	}

	/* Wait for SPU-thread to complete execution.  */
	for (i = 0; i < spu_threads; i++) {
		if (pthread_join (threads[i], NULL)) {
			perror("Failed pthread_join");
			exit (1);
		}
	}

	int pass = 1;
	for (i=0; i<PPU_ARR_SIZE;i++) {
		if (C[i] != 3) {
			pass = 0;
			printf("Array C: Result is incorrect. i = %d, expected 3, got %f\n", i, C[i]);
			break;
		}
	}
	if (pass) printf("Array C: Result is correct.\n");

	pass = 1;
	float d_rez[4] = {1, 5, 2, 4};
	for (i=0; i<PPU_ARR_SIZE;i++) {
		if (D[i] != d_rez[i % 4]) {
			printf("Array D: Result is incorrect. i = %d, expected %f, got %f\n",
				i, d_rez[i % 4], D[i]);
			pass = 0;
			break;
		}
	}
	if (pass) printf("Array D: Result is correct.\n");

	free_align(A);
	free_align(B);
	free_align(C);
	free_align(D);

	return 0;
}
