#include <stdio.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>
#include "../lab8_common.h"

/* TODO: adaugati define-ul de wait_tag (vezi exemple DMA) */
#define wait_tag(t) mfc_write_tag_mask(1<<t); mfc_read_tag_status_all();

void print_vector(vector float *v, int length);

int main(unsigned long long speid, unsigned long long argp, unsigned long long envp)
{
	size_t j=0;
	pointers_t p __attribute__ ((aligned(16)));
	uint32_t tag_id = mfc_tag_reserve();

	speid = speid;
	envp = envp;

	if (tag_id==MFC_TAG_INVALID){
		printf("SPU: ERROR can't allocate tag ID\n"); 
		return -1;
	}

	/* TODO 1: obtineti prin DMA structura cu pointeri */ 
	mfc_get((void*)&p, argp, sizeof(pointers_t), tag_id, 0, 0);
	wait_tag(tag_id);

	/* TODO 2: cititi in Local Store un set de date din A si unul din B */
	float A[SPU_ARR_SIZE] __attribute__ ((aligned(16)));
	float B[SPU_ARR_SIZE] __attribute__ ((aligned(16)));
	float C[SPU_ARR_SIZE] __attribute__ ((aligned(16)));
	float D[SPU_ARR_SIZE] __attribute__ ((aligned(16)));

	/* TODO 6: procesare mai mult seturi de date - for + adrese de pe PPU modificate */
	for (unsigned int i = 0; i < PPU_ARR_SIZE / SPU_ARR_SIZE / NUM_SPU_THREADS; i++) {
		/* TODO 2: cititi in Local Store un set de date din A si unul din B */
		mfc_get((void*)A, (uint32_t)(p.A) + i * DMA_TRANSFER_SIZE, DMA_TRANSFER_SIZE, tag_id, 0, 0);
		mfc_get((void*)B, (uint32_t)(p.B) + i * DMA_TRANSFER_SIZE, DMA_TRANSFER_SIZE, tag_id, 0, 0);
		wait_tag(tag_id);

		/* TODO 3: adunati element cu element A si B folosind operatii vectoriale */
		vector float *vA = (vector float*) A;
		vector float *vB = (vector float*) B;
		vector float *vC = (vector float*) C;
		vector float *vD = (vector float*) D;
		/* TODO 5: masti pentru array-ul D */
		vector unsigned char mask1 = {0, 1, 2, 3, 16, 17, 18, 19,  8,  9, 10, 11, 12, 13 ,14 ,15};
		vector unsigned char mask2 = {4, 5, 6, 7, 20, 21, 22, 23, 28, 29, 30, 31, 24, 25 ,26, 27}; 
		for (j=0; j < SPU_ARR_SIZE / (16 / sizeof(float)); j++) {
			/* TODO 3: adunati element cu element A si B folosind operatii vectoriale */
			vC[j] = vA[j] + vB[j]; 
			/* TODO 5: calcule efective pentru array-ul D */
			vD[j] = spu_shuffle(vA[j], vB[j], mask1) + spu_shuffle(vA[j], vB[j], mask2);
		}
		/* TODO 4: scrieti in main storage un set de date din C */
		mfc_put((void*)C, (uint32_t)(p.C) + i * DMA_TRANSFER_SIZE, DMA_TRANSFER_SIZE, tag_id, 0, 0);
		wait_tag(tag_id);

		/* TODO 5: scrieti in main storage un set de date din D */
		mfc_put((void*)D, (uint32_t)(p.D) + i * DMA_TRANSFER_SIZE, DMA_TRANSFER_SIZE, tag_id, 0, 0);
		wait_tag(tag_id);
	}
	/* eliberam tag id-ul */
	mfc_tag_release(tag_id);

	return 0;
}

void print_vector(vector float *v, int length)
{
	int i;
	for (i = 0; i < length; i+=1)
		printf("%.2lf %.2lf %.2lf %.2lf ", v[i][0], v[i][1], v[i][2], v[i][3]);
	printf("\n");
}
