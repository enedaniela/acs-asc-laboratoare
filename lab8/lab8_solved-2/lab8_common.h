typedef struct {
        float* A;       // pointer to section in first input array
        float* B;       // pointer to section in second input array
        float* C;       // pointer to section of first output array
        float* D;       // pointer to section of second output array
} pointers_t;

#define NUM_SPU_THREADS   (8)
#define DMA_TRANSFER_SIZE (16 * 1024)
#define SPU_ARR_SIZE      (DMA_TRANSFER_SIZE / sizeof(float))
//#define PPU_ARR_SIZE      (32768)
#define PPU_ARR_SIZE      (131072)

