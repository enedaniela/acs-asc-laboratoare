#include <stdio.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>
#include "../lab8_common.h"

/* TODO: adaugati define-ul de wait_tag (vezi exemple DMA) */
#define wait_tag(t) mfc_write_tag_mask(1<<t); mfc_read_tag_status_all();

void print_vector(vector float *v, int length);

int main(unsigned long long speid, unsigned long long argp, unsigned long long envp)
{
	uint32_t tag_id = mfc_tag_reserve();
	pointers_t str __attribute__ ((aligned(16)));

	/* silence warnings */
	speid = speid; 
	envp = envp;
	argp = argp;
	vector unsigned char pattern_A = (vector unsigned char){0, 1, 2, 3, 8,  9, 10, 11, 4, 5, 6, 7,4, 5, 6, 7};
	vector unsigned char pattern_B = (vector unsigned char){16,17,18,19,12,13,14,15,24,25,26,27,20,21,22,23};

	if (tag_id==MFC_TAG_INVALID){
		printf("SPU: ERROR can't allocate tag ID\n"); 
		return -1;
	}

	/* TODO 1: obtineti prin DMA structura cu pointeri */ 
	mfc_get((void*)&str, argp, sizeof(pointers_t), tag_id, 0, 0);
	wait_tag(tag_id);
	printf("ID %d ADRESE %p %p %p %p\n", speid, p.A, p.B, p.C, p.D);

	/* TODO 6: procesare mai mult seturi de date */

	/* TODO 2: cititi in Local Store un set de date din A si unul din B */
	float A[SPU_ARR_SIZE] __attribute__ ((aligned(16)));
	float B[SPU_ARR_SIZE] __attribute__ ((aligned(16)));
	float C[SPU_ARR_SIZE] __attribute__ ((aligned(16)));
	float D[SPU_ARR_SIZE] __attribute__ ((aligned(16)));

	vector float *va = (vector float *) A;
	vector float *vb = (vector float *) B;
	vector float *vc = (vector float *) C;
	vector float *vd = (vector float *) D;

	mfc_get((void*)A, (unsigned int)(str.A), DMA_TRANSFER_SIZE, tag_id, 0, 0);
	mfc_get((void*)B, (unsigned int)(str.B), DMA_TRANSFER_SIZE, tag_id, 0, 0);
	wait_tag(tag_id);

	/* TODO 3: inmultiti element cu element A si B folosind operatii vectoriale */
	
	int n = SPU_ARR_SIZE/(16/sizeof(float));
	for (i = 0; i < n; i++){     // N/4 iterații
	    vc[i] = spu_mul(va[i], vb[i]);

		/* TODO 5: calcule vectoriale pentru array-ul D */
		vd[i] = spu_shuffle(va[i], vb[i], pattern_A) + spu_shuffle(va[i], vb[i], pattern_B);
	}

	/* TODO 4: scrieti in main storage un set de date din C */
	mfc_put((void*)C, (uint32_t)(str.C), DMA_TRANSFER_SIZE, tag_id, 0, 0);
	wait_tag(tag_id);

	/* TODO 5: scrieti in main storage un set de date din D */
	mfc_put((void*)D, (uint32_t)(str.D), DMA_TRANSFER_SIZE, tag_id, 0, 0);
	wait_tag(tag_id);

	/* eliberam tag id-ul */
	mfc_tag_release(tag_id);

	return 0;
}

void print_vector(vector float *v, int length)
{
	int i;
	for (i = 0; i < length; i+=1)
		printf("%.2lf %.2lf %.2lf %.2lf ", v[i][0], v[i][1], v[i][2], v[i][3]);
	printf("\n");
}
