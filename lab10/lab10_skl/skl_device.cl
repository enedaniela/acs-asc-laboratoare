__kernel void kernel_id(__global float* output)
{
	uint gid = get_global_id(0);
	uint lid = get_local_id(0);
	//output[gid] = (float)gid + (float)lid * 0.01f;
	//output[gid] = (float)(gid%2);

	if(gid < 16)
		output[gid] = 1.0f;
	else
		output[gid] = 0.0f;


	// TODO ex4: modify to obtain patterns
	// PATTERN 1 - 0 and 1
	// PATTERN 2 - first 16 work-items 1 rest 0
}

__kernel void kernel_gflops(__global float* output)
{
	uint gid = get_global_id(0);
	uint lid = get_local_id(0);

	// TODO ex5 - Gflops perf
	output[gid] = 1.f;
}
