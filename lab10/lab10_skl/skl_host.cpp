#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "helper.hpp"

using namespace std;

/* used for buffer swap */
#define BUF_2M		(2 * 1024 * 1024)
#define BUF_32M		(32 * 1024 * 1024)

/* used for kernel execution */
#define BUF_128		(128)

/**
 * Retrieve GPU device
 */
void gpu_find(cl_device_id &device,
			  uint platform_select,
			  uint device_select)
{
	cl_platform_id platform;
	cl_uint platform_num = 0;
	cl_platform_id* platform_list = NULL;
	
	cl_uint device_num = 0;
	cl_device_id* device_list = NULL;
	
	size_t attr_size = 0;
	cl_char* attr_data = NULL;
	
	/* get num of available OpenCL platforms */
	CL_ERR( clGetPlatformIDs(0, NULL, &platform_num));
	platform_list = new cl_platform_id[platform_num];
	DIE(platform_list == NULL, "alloc platform_list");
	
	/* get all available OpenCL platforms */
	CL_ERR( clGetPlatformIDs(platform_num, platform_list, NULL));
	cout << "Platforms found: " << platform_num << endl;
	
	/* list all platforms and VENDOR/VERSION properties */
	for(uint platf=0; platf<platform_num; platf++)
	{
		/* get attribute CL_PLATFORM_VENDOR */
		CL_ERR( clGetPlatformInfo(platform_list[platf],
			CL_PLATFORM_VENDOR, 0, NULL, &attr_size));
		attr_data = new cl_char[attr_size];
		DIE(attr_data == NULL, "alloc attr_data");
		
		/* get data CL_PLATFORM_VENDOR */
		CL_ERR( clGetPlatformInfo(platform_list[platf],
			CL_PLATFORM_VENDOR, attr_size, attr_data, NULL));
		cout << "Platform " << platf << " " << attr_data << " ";
		delete[] attr_data;
		
		/* get attribute size CL_PLATFORM_VERSION */
		// TODO ex1 - get and display OpenCL platform version
		/* get attribute size CL_PLATFORM_VERSION */
		CL_ERR( clGetPlatformInfo(platform_list[platf],
				CL_PLATFORM_VERSION, 0, NULL, &attr_size));
		attr_data = new cl_char[attr_size];
		DIE(attr_data == NULL, "alloc attr_data");

		/* get data size CL_PLATFORM_VERSION */
		CL_ERR( clGetPlatformInfo(platform_list[platf],
				CL_PLATFORM_VERSION, attr_size, attr_data, NULL));
		cout <<"Platform version "<<attr_data << endl;
		delete[] attr_data;
		
		/* no valid platform found */
		platform = platform_list[platf];
		DIE(platform == 0, "platform selection");
		
		/* get num of available OpenCL devices type GPU on the selected platform */
		CL_ERR( clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &device_num));
		device_list = new cl_device_id[device_num];
		DIE(device_list == NULL, "alloc devices");
		
		/* get all available OpenCL devices type GPU on the selected platform */
		// TODO ex1 - filter and get only GPU type devices
		CL_ERR( clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,
			device_num, device_list, NULL));
		cout << "\tDevices found " << device_num  << endl;

		
		/* list all devices and TYPE/VERSION properties */
		for(uint dev=0; dev<device_num; dev++)
		{
			/* get attribute size */
			// TODO ex1 - get and display CL_DEVICE_NAME
			// TODO ex1 - get and display CL_DEVICE_VERSION
			
			/* get attribute size */
			CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_NAME,
					0, NULL, &attr_size));
			attr_data = new cl_char[attr_size];
			DIE(attr_data == NULL, "alloc attr_data");

			/* get attribute CL_DEVICE_NAME */
			CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_NAME,
					attr_size, attr_data, NULL));
			cout << "Device " << dev << " " << attr_data << " ";
			delete[] attr_data;

			/* get attribute size */
			CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_VERSION,
					0, NULL, &attr_size));
			attr_data = new cl_char[attr_size];
			DIE(attr_data == NULL, "alloc attr_data");

			/* get attribute CL_DEVICE_VERSION */
			CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_VERSION,
					attr_size, attr_data, NULL));
			cout << attr_data << endl;
			delete[] attr_data;

			/* select device based on cli arguments */
			if((platf == platform_select) && (dev == device_select)){
				device = device_list[dev];
				cout << " <--- SELECTED ";
			}
			
			cout << endl;
		}
	}
	
	delete[] platform_list;
	delete[] device_list;
}

/**
* Swap buffers using the select device
*/
void gpu_swap_buffers(cl_device_id device)
{
	cl_int ret;

	cl_context context;
	cl_command_queue cmd_queue;

	/* create a context for the device */
	context = clCreateContext(0, 1, &device, NULL, NULL, &ret);
	CL_ERR( ret );

	/* create a command queue for the device in the context */
	cmd_queue = clCreateCommandQueue(context, device, 0, &ret);
	CL_ERR( ret );

	/* allocate 2 buffers of 32 MB of data on the CPU (HOST) */
	cl_char *buf1_host = new cl_char[BUF_32M];
		DIE ( buf1_host == NULL, "alloc buf_host1" );
	cl_char *buf2_host = new cl_char[BUF_32M];
		DIE ( buf2_host == NULL, "alloc buf_host1" );

	/* fill the CPU buffer data */
	for(int i = 0; i < BUF_32M; i++ ){
		buf1_host[i] = 1;
		buf2_host[i] = 2;
	}

	/* print some values from CPU buffers */
	cout << "BEFORE swap " << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	 	cout << (int) buf1_host[i] << " ";
	cout << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
		cout << (int) buf2_host[i] << " ";
	cout << endl;

	// TODO ex2: allocate 2 buffers on GPU, using clCreatebuf, each BUF_32M size
	cl_mem buf1_dev = 0;
	cl_mem buf2_dev = 0;
	buf1_dev = clCreateBuffer (context, CL_MEM_ALLOC_HOST_PTR, BUF_32M, NULL, &ret);
	buf2_dev = clCreateBuffer (context, CL_MEM_ALLOC_HOST_PTR, BUF_32M, NULL, &ret);

	// TODO ex2: allocate 1 smaller buffer GPU, using clCreatebuf, BUF_2M size
	cl_mem buf3_dev = 0;
	buf3_dev = clCreateBuffer (context, CL_MEM_ALLOC_HOST_PTR, BUF_2M, NULL, &ret);

	DIE(buf1_dev == 0, "alloc buf_dev1");
	DIE(buf2_dev == 0, "alloc buf_dev2");
	DIE(buf3_dev == 0, "alloc buf_dev3");

	/* copy initial buffers to the GPU device */
	CL_ERR( clEnqueueWriteBuffer(cmd_queue, buf1_dev, CL_TRUE, 0,
		  sizeof(char) * BUF_32M, buf1_host, 0, NULL, NULL));
	CL_ERR( clEnqueueWriteBuffer(cmd_queue, buf2_dev, CL_TRUE, 0,
		  sizeof(char) * BUF_32M, buf2_host, 0, NULL, NULL));

	/* interchange the buffers using the OpenCL API HOST functions on the GPU */
	for( int i = 0; i < (BUF_32M / BUF_2M); i++ ){
		size_t offset = sizeof(char) * i * BUF_2M;

		// TODO ex2: interchange the buffers using the OpenCL API HOST functions
		// e.g. clEnqueueCopybuf
		CL_ERR( clEnqueueCopyBuffer(cmd_queue, buf1_dev, buf3_dev, offset, 0, sizeof(char) * BUF_2M, 0, NULL, NULL));
		CL_ERR( clEnqueueCopyBuffer(cmd_queue, buf2_dev, buf1_dev, offset, offset, sizeof(char) * BUF_2M, 0, NULL, NULL));
		CL_ERR( clEnqueueCopyBuffer(cmd_queue, buf3_dev, buf2_dev, 0, offset, sizeof(char) * BUF_2M, 0, NULL, NULL));
	}

	// TODO ex2: copy the interchanged buffers back to HOST
	// buf1_dev => bu1f_host, buf2_dev => buf2_host
	CL_ERR( clEnqueueReadBuffer(cmd_queue, buf1_dev, CL_TRUE, 0, sizeof(char) * BUF_32M, buf1_host, 0, NULL, NULL));
	CL_ERR( clEnqueueReadBuffer(cmd_queue, buf2_dev, CL_TRUE, 0, sizeof(char) * BUF_32M, buf2_host, 0, NULL, NULL));

	/* print some values from CPU buffers (after interchange/copy from GPU) */
	cout << "AFTER swap " << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	 	cout << (int) buf1_host[i] << " ";
	cout << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	 	cout << (int) buf2_host[i] << " ";
	cout << endl;

	/* wait for all enqueued operations to finish */
	CL_ERR( clFinish(cmd_queue) );

	/* free all resources related to GPU */
	CL_ERR( clReleaseMemObject(buf1_dev) );
	CL_ERR( clReleaseMemObject(buf2_dev) );
	CL_ERR( clReleaseMemObject(buf3_dev) );
	CL_ERR( clReleaseCommandQueue(cmd_queue) );
	CL_ERR( clReleaseContext(context) );

	/* free all resources on CPU */
	delete[] buf1_host;
	delete[] buf2_host;
}


/**
* Exec kernel using the select device
*/
void gpu_execute_kernel(cl_device_id device, char* nume)
{
	cl_int ret;

	cl_context context;
	cl_command_queue cmd_queue;
	cl_program program;
	cl_kernel kernel;

	string kernel_src;
	struct timeval start, stop; 
	gettimeofday(&start, NULL);
	/* create a context for the device */
	context = clCreateContext(0, 1, &device, NULL, NULL, &ret);
	CL_ERR( ret );

	/* create a command queue for the device in the context */
	cmd_queue = clCreateCommandQueue(context, device, 0, &ret);
	CL_ERR( ret );

	/* allocate 1 buffer of BUF_128 float elements on the CPU (HOST) */
	cl_float *buf_host = new cl_float[BUF_128];
	DIE ( buf_host == NULL, "alloc buf_host1" );

	/* allocate 1 buffer of BUF_128 float elements on the GPU (DEVICE)*/
	cl_mem buf_dev = clCreateBuffer(context, CL_MEM_READ_WRITE,
				  sizeof(cl_float) * BUF_128, NULL, &ret);
	CL_ERR( ret );

	/* retrieve kernel source */
	read_kernel("skl_device.cl", kernel_src);
	const char* kernel_c_str = kernel_src.c_str();

	/* create kernel program from source */
	program = clCreateProgramWithSource(context, 1,
		  &kernel_c_str, NULL, &ret);
	CL_ERR( ret );

	/* compile the program for the given set of devices */
	ret = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	CL_COMPILE_ERR( ret, program, device );

	/* create kernel associated to compiled source kernel */
	kernel = clCreateKernel(program, nume, &ret);
	CL_ERR( ret );

	// TODO ex3: set kernel arguments using clSetKernelArg
	clSetKernelArg (kernel, 0, sizeof(cl_mem), (void *)&buf_dev);

	// TODO ex3: adjust the globalSize/localSize to cover all the buffer exec BUF_128
	// TODO ex3: experiment with different NDRANGES
	// TODO ex4: modify to obtain patterns
	size_t globalSize[2] = {BUF_128, 0};
	size_t localSize[2] = {BUF_128, 0};

	// TODO ex3: call clEnqueueNDRangeKernel with globalSize/localSize
	 clEnqueueNDRangeKernel(cmd_queue, kernel, 1, NULL, globalSize, localSize, 0, NULL, NULL);
	/* copy the buffers back */
	CL_ERR( clEnqueueReadBuffer(cmd_queue, buf_dev, CL_TRUE, 0,
		  sizeof(float) * BUF_128, buf_host, 0, NULL, NULL));

	/* print some values from CPU buffers (after GPU compute) */
	for(int i = 0; i < BUF_128; i++)
		cout << (float) buf_host[i] << " ";
	cout << endl;

	/* wait for all enqueued operations to finish */
	CL_ERR( clFinish(cmd_queue) );
	gettimeofday(&stop, NULL);

	long long msstart = (long long) start.tv_sec * 1000L + start.tv_usec / 1000; //get current timestamp in milliseconds
	long long msstop = (long long) stop.tv_sec * 1000L + stop.tv_usec / 1000; //get current timestamp in milliseconds
    std::cout << msstop - msstart << std::endl;

	/* free all resources related to GPU */
	CL_ERR( clReleaseMemObject(buf_dev) );
	CL_ERR( clReleaseCommandQueue(cmd_queue) );
	CL_ERR( clReleaseContext(context) );

	/* free all resources on CPU */
	delete[] buf_host;
}

/**
* MAIN function (CPU/HOST)
*/
int main(int argc, char** argv)
{
	cl_device_id device;
	int platform_select = 0;
	int device_select = 0;
	
	if(argc == 3){
		platform_select = atoi(argv[1]);
		device_select = atoi(argv[2]);
	}
	
	/* list selected platform and devices */
	if(argc != 3)
		cout << "./bin <platform> <device>" << endl << endl;
	cout << "SELECT(platform = " << platform_select << ", device = " << device_select << ")" << endl;

	/* retrieve platform and device (GPU NVIDIA TESLA) */
	cout << endl << " -- EX 1 -- " << endl;
	gpu_find(device, platform_select, device_select);
	DIE(device == 0, "check valid device");

	/* perform buffer swap using selected device (GPU NVIDIA TESLA) */
	cout << endl << " -- EX 2 -- " << endl;
	gpu_swap_buffers(device);

	/* perform kernel execution using selected device (GPU NVIDIA TESLA) */
	char* nume1 = strdup("kernel_id");
	cout << endl << " -- EX 3 -- " << endl;
	gpu_execute_kernel(device, nume1);

	char* nume2 = strdup("kernel_gflops");
	cout << endl << " -- EX 5 -- " << endl;
	gpu_execute_kernel(device, nume2);

	return 0;
}
