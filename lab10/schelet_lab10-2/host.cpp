#include <stdio.h>
#include <CL/cl.h>
#include <iostream>
#include <string>

#include "helper.hpp"

using namespace std;

/* used for buffer swap */
#define BUF_2M		(2 * 1024 * 1024)
#define BUF_32M		(32 * 1024 * 1024)

/* used for kernel execution */
#define BUF_128		(128)

/**
* Retrieve GPU device
*/
void gpu_find(cl_device_id &device)
{
	cl_platform_id platform;
	cl_uint platform_num = 0;
	cl_platform_id* platform_list = NULL;

	cl_uint device_num = 0;
	cl_device_id* device_list = NULL;

	size_t attr_size = 0;
	cl_char* attr_data = NULL;

	/* get num of available OpenCL platforms */
	CL_ERR( clGetPlatformIDs(0, NULL, &platform_num));
	platform_list = new cl_platform_id[platform_num];
	DIE(platform_list == NULL, "alloc platform_list");

	/* get all available OpenCL platforms */
	CL_ERR( clGetPlatformIDs(platform_num, platform_list, NULL));
	cout << "Found " << platform_num << " platform(s) "<< endl;

	/* list all platforms and VENDOR/VERSION properties */
	for(uint i=0; i<platform_num; i++)
	{
		/* get attribute CL_PLATFORM_VENDOR */
		CL_ERR( clGetPlatformInfo(platform_list[i],
				CL_PLATFORM_VENDOR, 0, NULL, &attr_size));
		attr_data = new cl_char[attr_size];
		DIE(attr_data == NULL, "alloc attr_data");

		/* get data CL_PLATFORM_VENDOR */
		CL_ERR( clGetPlatformInfo(platform_list[i],
				CL_PLATFORM_VENDOR, attr_size, attr_data, NULL));
		cout << "Platform " << i << " " << attr_data << " ";

		// TODO ex1 : modify call to select NVIDIA platform based on attr_data
		platform = 0;

		delete[] attr_data;

		/* get attribute size CL_PLATFORM_VERSION */
		CL_ERR( clGetPlatformInfo(platform_list[i],
				CL_PLATFORM_VERSION, 0, NULL, &attr_size));
		attr_data = new cl_char[attr_size];
		DIE(attr_data == NULL, "alloc attr_data");

		/* get data size CL_PLATFORM_VERSION */
		CL_ERR( clGetPlatformInfo(platform_list[i],
				CL_PLATFORM_VERSION, attr_size, attr_data, NULL));
		cout << attr_data << endl;
		delete[] attr_data;
	}

	/* no platform found */
	DIE(platform == 0, "platform selection");

	// TODO ex1 : modify bellow call to get only GPU type devices
	/* get num of available OpenCL devices type GPU on the selected platform */
	CL_ERR( clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &device_num));
	device_list = new cl_device_id[device_num];
	DIE(device_list == NULL, "alloc devices");

	/* get all available OpenCL devices type ALL on the selected platform */
	CL_ERR( clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,
		  device_num, device_list, NULL));
	cout << "Found " << device_num << " device(s)" << endl;

	/* list all devices and TYPE/VERSION properties */
	for(uint i=0; i<device_num; i++)
	{
		// TODO ex1 : call clGetDeviceInfo, get CL_DEVICE_NAME

		// TODO ex1 : call clGetDeviceInfo, get CL_DEVICE_VERSION
	}

	/* select first available GPU */
	if(device_num > 0)
		device = device_list[0];
	else
		device = 0;

	delete[] platform_list;
	delete[] device_list;
}

/**
* Swap buffers using the select device
*/
void gpu_swap_buffers(cl_device_id device)
{
	cl_int ret;

	cl_context context;
	cl_command_queue cmd_queue;

	/* create a context for the device */
	context = clCreateContext(0, 1, &device, NULL, NULL, &ret);
	CL_ERR( ret );

	/* create a command queue for the device in the context */
	cmd_queue = clCreateCommandQueue(context, device, 0, &ret);
	CL_ERR( ret );

	/* allocate 2 buffers of 32 MB of data on the CPU (HOST) */
	cl_char *buf1_host = new cl_char[BUF_32M];
	DIE ( buf1_host == NULL, "alloc buf_host1" );
	cl_char *buf2_host = new cl_char[BUF_32M];
	DIE ( buf2_host == NULL, "alloc buf_host1" );

	/* fill the CPU buffer data */
	for(int i = 0; i < BUF_32M; i++ ){
		buf1_host[i] = 1;
		buf2_host[i] = 2;
	}

	/* print some values from CPU buffers */
	cout << "BEFORE swap " << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	  cout << (int) buf1_host[i] << " ";
	cout << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	  cout << (int) buf2_host[i] << " ";
	cout << endl;

	// TODO ex2: allocate 2 buffers on GPU, using clCreatebuf, each BUF_32M size
	cl_mem buf1_dev = 0;
	cl_mem buf2_dev = 0;

	// TODO ex2: allocate 1 smaller buffer GPU, using clCreatebuf, BUF_2M size
	cl_mem buf3_dev = 0;

	DIE(buf1_dev == 0, "alloc buf_dev1");
	DIE(buf2_dev == 0, "alloc buf_dev2");
	DIE(buf3_dev == 0, "alloc buf_dev3");

	/* copy initial buffers to the GPU device */
	CL_ERR( clEnqueueWriteBuffer(cmd_queue, buf1_dev, CL_TRUE, 0,
		  sizeof(char) * BUF_32M, buf1_host, 0, NULL, NULL));
	CL_ERR( clEnqueueWriteBuffer(cmd_queue, buf2_dev, CL_TRUE, 0,
		  sizeof(char) * BUF_32M, buf2_host, 0, NULL, NULL));

	/* interchange the buffers using the OpenCL API HOST functions on the GPU */
	for( int i = 0; i < (BUF_32M / BUF_2M); i++ ){
		size_t offset = sizeof(char) * i * BUF_2M;

		// TODO ex2: interchange the buffers using the OpenCL API HOST functions
		// e.g. clEnqueueCopybuf
	}

	// TODO ex2: copy the interchanged buffers back to HOST
	// buf1_dev => bu1f_host, buf2_dev => buf2_host

	/* print some values from CPU buffers (after interchange/copy from GPU) */
	cout << "AFTER swap " << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	  cout << (int) buf1_host[i] << " ";
	cout << endl;
	for(int i = 0; i < BUF_32M; i += (BUF_32M / 16))
	  cout << (int) buf2_host[i] << " ";
	cout << endl;

	/* wait for all enqueued operations to finish */
	CL_ERR( clFinish(cmd_queue) );

	/* free all resources related to GPU */
	CL_ERR( clReleaseMemObject(buf1_dev) );
	CL_ERR( clReleaseMemObject(buf2_dev) );
	CL_ERR( clReleaseMemObject(buf3_dev) );
	CL_ERR( clReleaseCommandQueue(cmd_queue) );
	CL_ERR( clReleaseContext(context) );

	/* free all resources on CPU */
	delete[] buf1_host;
	delete[] buf2_host;
}


/**
* Exec kernel using the select device
*/
void gpu_execute_kernel(cl_device_id device)
{
	cl_int ret;

	cl_context context;
	cl_command_queue cmd_queue;
	cl_program program;
	cl_kernel kernel;

	string kernel_src;

	/* create a context for the device */
	context = clCreateContext(0, 1, &device, NULL, NULL, &ret);
	CL_ERR( ret );

	/* create a command queue for the device in the context */
	cmd_queue = clCreateCommandQueue(context, device, 0, &ret);
	CL_ERR( ret );

	/* allocate 1 buffer of BUF_128 float elements on the CPU (HOST) */
	cl_float *buf_host = new cl_float[BUF_128];
	DIE ( buf_host == NULL, "alloc buf_host1" );

	/* allocate 1 buffer of BUF_128 float elements on the GPU (DEVICE)*/
	cl_mem buf_dev = clCreateBuffer(context, CL_MEM_READ_WRITE,
				  sizeof(cl_float) * BUF_128, NULL, &ret);
	CL_ERR( ret );

	/* retrieve kernel source */
	read_kernel("device.cl", kernel_src);
	const char* kernel_c_str = kernel_src.c_str();

	/* create kernel program from source */
	program = clCreateProgramWithSource(context, 1,
		  &kernel_c_str, NULL, &ret);
	CL_ERR( ret );

	/* compile the program for the given set of devices */
	ret = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	CL_COMPILE_ERR( ret, program, device );

	/* create kernel associated to compiled source kernel */
	kernel = clCreateKernel(program, "kernel_id", &ret);
	CL_ERR( ret );

	// TODO ex3: set kernel arguments using clSetKernelArg

	// TODO ex3: adjust the globalSize/localSize to cover all the buffer exec BUF_128
	// TODO ex3: experiment with different NDRANGES
	// TODO ex4: modify to obtain patterns
	size_t globalSize[2] = {1, 0};
	size_t localSize[2] = {1, 0};

	// TODO ex3: call clEnqueueNDRangeKernel with globalSize/localSize

	/* copy the buffers back */
	CL_ERR( clEnqueueReadBuffer(cmd_queue, buf_dev, CL_TRUE, 0,
		  sizeof(float) * BUF_128, buf_host, 0, NULL, NULL));

	/* print some values from CPU buffers (after GPU compute) */
	for(int i = 0; i < BUF_128; i++)
		cout << (float) buf_host[i] << " ";
	cout << endl;

	/* wait for all enqueued operations to finish */
	CL_ERR( clFinish(cmd_queue) );

	/* free all resources related to GPU */
	CL_ERR( clReleaseMemObject(buf_dev) );
	CL_ERR( clReleaseCommandQueue(cmd_queue) );
	CL_ERR( clReleaseContext(context) );

	/* free all resources on CPU */
	delete[] buf_host;
}

/**
* MAIN function (CPU/HOST)
*/
int main(int argc, char** argv)
{
	cl_device_id device;

	/* retrieve platform and device (GPU NVIDIA TESLA) */
	cout << endl << " -- EX 1 -- " << endl;
	gpu_find(device);
	DIE(device == 0, "check valid device");

	/* perform buffer swap using selected device (GPU NVIDIA TESLA) */
	cout << endl << " -- EX 2 -- " << endl;
	gpu_swap_buffers(device);

	/* perform kernel execution using selected device (GPU NVIDIA TESLA) */
	cout << endl << " -- EX 3 -- " << endl;
	gpu_execute_kernel(device);

	return 0;
}
