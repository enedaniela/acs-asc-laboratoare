__kernel void kernel_id(__global float* output)
{
	uint gid = get_global_id(0);
	uint lid = get_local_id(0);
	output[gid] = (float)gid + (float)lid * 0.01f;

/* PATTERN 1 - 0 and 1 */
	//output[gid] = (float) ( gid % 2);

/* PATTERN 2 - first 16 work-items 1 rest 0 */
	//output[gid] = (float) ( gid < 16 ? 1 : 0);
}
